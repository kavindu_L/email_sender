name := "email_sender"

version := "1.0.0"

scalaVersion := "2.12.7"


libraryDependencies ++= Seq(
  //// https://mvnrepository.com/artifact/javax.mail/mail
  //libraryDependencies += "javax.mail" % "mail" % "1.4.7",

  // https://mvnrepository.com/artifact/com.sun.mail/javax.mail
  "com.sun.mail" % "javax.mail" % "1.6.2",

  // https://mvnrepository.com/artifact/javax.activation/activation
  "javax.activation" % "activation" % "1.1.1",

  // https://mvnrepository.com/artifact/org.json/json
  "org.json" % "json" % "20180813",

  "com.typesafe.akka" %% "akka-actor" % "2.5.21",

  "log4j" % "log4j" % "1.2.14"

)


