import java.io.File
import java.util.Properties

import javax.activation.{CommandMap, DataHandler, FileDataSource, MailcapCommandMap}
import javax.mail.internet.{InternetAddress, MimeBodyPart, MimeMessage, MimeMultipart}
import javax.mail.{Authenticator, Message, MessagingException, PasswordAuthentication, Session, Transport}
import org.apache.log4j.Logger
import org.json.JSONObject

import scala.collection.mutable.ListBuffer

object emailSender {

  val log = Logger.getLogger(this.getClass.getName)

  def main(args: Array[String]): Unit = {

    val to = "sample@gmail.com"
    val body = "email_body"
    val cc_data ="sample@gmail.com"
    val subject = "email_title"
    val fileName = ListBuffer("photo.jpg")
    val js ="{\"pdfName\":\"The Linux Command Line.pdf\"}"

    mailSender(to,body,cc_data,subject,fileName,js)


  }

  def mailSender(to : String,
                 body: String,
                 cc_data:String,
                 subject: String,
                 filePathList: ListBuffer[String],
                 jsonMessage:String): Unit ={

    try{

      val documentObj = new JSONObject(jsonMessage)
//      val reportType = documentObj.getInt("reportType")
//      val status = documentObj.getString("status")

      if(checkFileSize(filePathList(0))(0).toString.toBoolean){
        log.info(s"file size is within the range")
        log.info(s"attach file to the mail...")
        val text = "Please find the attachment"

        setMails(to,cc_data,subject,body,filePathList,text)


      }else{
        val text = "File size exceeds the size limit"
        sendErrorMail(to,cc_data,subject,body,filePathList,text)
      }


    }catch {
      case e: MessagingException =>
        e.printStackTrace
        log.error(s"$e")
    }
  }

  /**
   * check the size of the given file
   *
   * */

  def checkFileSize(fileName: String): Array[Any]={

    log.info(s"check attachment file size")

    val sizeReturnVal = new Array[Any](2)
    val fullFilePath = applicationConfigs.basePath + fileName

    val file = new File(fullFilePath)

    /**get file size*/
    val byt: Double = file.length
    val kiloBytes: Double  = byt / 1024
    val megaBytes: Double  = kiloBytes / 1024

    if(megaBytes <= 25) {
      sizeReturnVal(0) = true
      sizeReturnVal(1) = megaBytes
      sizeReturnVal
    }
    else {
      sizeReturnVal(0) = false
      sizeReturnVal(1) = megaBytes
      sizeReturnVal
    }

  }


  /**
   * ready the mails to be sent
   *
   * */

  def setMails(to: String,
               cc_data: String,
               subject: String,
               body: String,
               fileNameList: ListBuffer[String],
               text : String): Unit ={

    val host = applicationConfigs.host
    val user = applicationConfigs.user
    val password = applicationConfigs.password
    val SSL_FACTORY =applicationConfigs.SSL_FACTORY
    val port =applicationConfigs.port
    val basePath = applicationConfigs.basePath


    log.info(s"get the session object")

    /***
     * 1)Get the session object
     * */

    val props = new Properties()
    props.put("mail.smtp.host", host)
    props.put("mail.smtp.auth", "true")
    props.put("mail.smtp.port", port)
    props.put("mail.smtp.socketFactory.port", port)
    props.put("mail.smtp.socketFactory.class", SSL_FACTORY)

    val session = Session.getDefaultInstance(props, new Authenticator() {
      override protected def getPasswordAuthentication = new PasswordAuthentication(user, password)
    })

    log.info("compose the message")

    /**
     * 2)Compose the message
     *
     * */

    try{

      val message = new MimeMessage(session)
      message.setFrom(new InternetAddress(user))
      message.addRecipient(Message.RecipientType.TO, new InternetAddress(to))

      message.setSubject(subject)
      cc_data.split(",").map(_.trim).foreach{
        r => message.addRecipient(Message.RecipientType.CC, new InternetAddress(r))
      }


      /**
       * set body and attachment
       *
       * */

      /**
       * 3) create MimeBodyPart object and set your message text
       * */
      val messageBodyPart = new MimeBodyPart()
      messageBodyPart.setText(s"Hello, $body \n")


      val messageBodyPart1 = new MimeBodyPart()
      messageBodyPart1.setText(text)

      /**
       * 4) create new MimeBodyPart object and set DataHandler object to this object
       * */

      val bodyPartsList : ListBuffer[MimeBodyPart] = ListBuffer()

      fileNameList.foreach{
        r =>
          val fullPath = basePath+r
          val messageBodyPart2 = new MimeBodyPart()
          val source = new FileDataSource(fullPath)
          messageBodyPart2.setDataHandler(new DataHandler(source))
          messageBodyPart2.setFileName(fullPath)
          bodyPartsList+= messageBodyPart2
      }


      /**
       * 5) create Multipart object and add MimeBodyPart objects to this object
       * */
      val multipart = new MimeMultipart()
      multipart.addBodyPart(messageBodyPart)
      multipart.addBodyPart(messageBodyPart1)

      bodyPartsList.foreach{
        r =>
          multipart.addBodyPart(r)
      }

      // Original answer from Som:// Original answer from Som:
      val mc = CommandMap.getDefaultCommandMap.asInstanceOf[MailcapCommandMap]
      mc.addMailcap("text/html;; x-java-content-handler=com.sun.mail.handlers.text_html")
      mc.addMailcap("text/xml;; x-java-content-handler=com.sun.mail.handlers.text_xml")
      mc.addMailcap("text/plain;; x-java-content-handler=com.sun.mail.handlers.text_plain")
      mc.addMailcap("multipart/*;; x-java-content-handler=com.sun.mail.handlers.multipart_mixed")
      mc.addMailcap("message/rfc822;; x-java-content-handler=com.sun.mail.handlers.message_rfc822")
      CommandMap.setDefaultCommandMap(mc)


      // Additional elements to make DSN work // Additional elements to make DSN work
      mc.addMailcap("multipart/report;;  x-java-content-handler=com.sun.mail.dsn.multipart_report")
      mc.addMailcap("message/delivery-status;; x-java-content-handler=com.sun.mail.dsn.message_deliverystatus")
      mc.addMailcap("message/disposition-notification;; x-java-content-handler=com.sun.mail.dsn.message_dispositionnotification")
      mc.addMailcap("text/rfc822-headers;;   x-java-content-handler=com.sun.mail.dsn.text_rfc822headers")


      /**
       * 6) set the multipart object to the message object
       * */
      message.setContent(multipart)


      /**send the message*/
      Transport.send(message)
      log.info("message sent successfully...")

    }catch {
      case e: MessagingException =>
        log.error(s"$e")

    }

  }

  /**
   * send mail when attachment exceed the size limit or fail to generate
   *
   * */

  def sendErrorMail(to: String,
                    cc_data: String,
                    subject: String,
                    body: String,
                    fileNameList: ListBuffer[String],
                    text : String)={

    val host = applicationConfigs.host
    val user = applicationConfigs.user
    val password = applicationConfigs.password
    val SSL_FACTORY =applicationConfigs.SSL_FACTORY
    val port =applicationConfigs.port
    val basePath = applicationConfigs.basePath


    log.info("file cannot be attached due to large file size")

    /***
     * 1)Get the session object
     * */

    val props = new Properties()
    props.put("mail.smtp.host", host)
    props.put("mail.smtp.auth", "true")
    props.put("mail.smtp.port", port)
    props.put("mail.smtp.socketFactory.port", port)
    props.put("mail.smtp.socketFactory.class", SSL_FACTORY)

    val session = Session.getDefaultInstance(props, new Authenticator() {
      override protected def getPasswordAuthentication = new PasswordAuthentication(user, password)
    })


    /**
     * 2)Compose the message
     *
     * */

    try{

      val message = new MimeMessage(session)
      message.setFrom(new InternetAddress(user))
      message.addRecipient(Message.RecipientType.TO, new InternetAddress(to))

      cc_data.split(",").map(_.trim).foreach{
        r => message.addRecipient(Message.RecipientType.CC, new InternetAddress(r))
      }


      message.setSubject("Error : "+text)

      /**
       * 3) create MimeBodyPart object and set your message text
       * */

      val messageBodyPart = new MimeBodyPart()
      messageBodyPart.setText("Your file cannot be attached due to large file size.\n")

      val bodyPartsList : ListBuffer[MimeBodyPart] = ListBuffer()

      if(fileNameList.nonEmpty){

        fileNameList.foreach{
          r =>

            val fullFilePath = basePath + r
            val html = r + s"\t<a href='$fullFilePath'> filePath</a> \n"

            val messageBodyPart1 = new MimeBodyPart()
            messageBodyPart1.setText(html, "UTF-8", "html")
            bodyPartsList+= messageBodyPart1


        }


        /**
         * 5) create Multipart object and add MimeBodyPart objects to this object
         * */
        val multipart = new MimeMultipart()
        multipart.addBodyPart(messageBodyPart)

        bodyPartsList.foreach{
          r =>
            multipart.addBodyPart(r)
        }


        // Original answer from Som:// Original answer from Som:
        val mc = CommandMap.getDefaultCommandMap.asInstanceOf[MailcapCommandMap]
        mc.addMailcap("text/html;; x-java-content-handler=com.sun.mail.handlers.text_html")
        mc.addMailcap("text/xml;; x-java-content-handler=com.sun.mail.handlers.text_xml")
        mc.addMailcap("text/plain;; x-java-content-handler=com.sun.mail.handlers.text_plain")
        mc.addMailcap("multipart/*;; x-java-content-handler=com.sun.mail.handlers.multipart_mixed")
        mc.addMailcap("message/rfc822;; x-java-content-handler=com.sun.mail.handlers.message_rfc822")
        CommandMap.setDefaultCommandMap(mc)


        // Additional elements to make DSN work // Additional elements to make DSN work
        mc.addMailcap("multipart/report;;  x-java-content-handler=com.sun.mail.dsn.multipart_report")
        mc.addMailcap("message/delivery-status;; x-java-content-handler=com.sun.mail.dsn.message_deliverystatus")
        mc.addMailcap("message/disposition-notification;; x-java-content-handler=com.sun.mail.dsn.message_dispositionnotification")
        mc.addMailcap("text/rfc822-headers;;   x-java-content-handler=com.sun.mail.dsn.text_rfc822headers")



        /**
         * 7) set the multipart object to the message object
         * */
        message.setContent(multipart)
      }else{
        message.setText("file cannot generate")
      }



      /**send the message*/
      Transport.send(message)
      log.info("error message sent successfully...")

    }catch {
      case e: MessagingException =>
        log.error(s"$e")

    }



  }
}
