import java.io.File

import com.typesafe.config.ConfigFactory
import org.apache.log4j.PropertyConfigurator

object applicationConfigs {

    val Conf = ConfigFactory.parseFile(new File(s"${System.getProperty("user.dir")}/configs/application.conf"))
    PropertyConfigurator.configure(s"${System.getProperty("user.dir")}/configs/log4j.properties")

    // Enviroment
    val environment:String = sys.env.getOrElse("ENV",Conf.getString("app-env"))

    val host = Conf.getString(s"${environment}.email.host")
    val user = Conf.getString(s"${environment}.email.user")
    val password = Conf.getString(s"${environment}.email.password")
    val SSL_FACTORY =Conf.getString(s"${environment}.email.SSL_FACTORY")
    val port =Conf.getString(s"${environment}.email.port")

    val basePath = Conf.getString(s"${environment}.basePath")
}
